# Modeling Cortex-M55 with Corstone-300 FVP
This image contains the Corstone-300 Fixed Virtual Platform (FVP) model which can be used to run projects compiled and targeted to the Cortex-M55 processor.

## Usage
This image will run projects compiled with Arm Compiler 6 against the Corstone-300 model, the compiled hex file needs to be included in the repository that the docker container will clone and run with the model. For example in the `Cortex-M55 Hello World` project, the hex file is located at `cortex-m55-hello-world/cortex-m55/Objects/Blinky.axf`.

## Dependencies
 * Ubuntu 18.04 (From buildpack-deps:18.04)
 * Curl (From buildpack-deps:18.04)
 * Unzip (From buildpack-deps:18.04)
 * libdbus-1-3
 * xvfb
 * netcat
 * expect
 * tcl
 * Arm Corstone SSE-300 FVP Model

## License
Copyright 2020 Dojo Five LLC
